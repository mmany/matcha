Matcha est un site de rencontre humains/Androids sur le theme de Detroit Become Human.

Ce site s'appelle donc Jerichonor (clin d'oeil aux connaisseurs) !

Les étapes pour visualiser le site  :
- clone le repository
- modifier votre mot de passe de base de données dans config/database.php
(ligne "$DB_PASSWORD = 'mmany17';")
- executer config/setup.php (possibilité de le faire directement depuis le navigateur
via "http://localhost:8080/config/setup.php" si vous avez les réglages par défaut
de PhpMyAdmin)
- executer config/test.php (possibilité de le faire directement depuis le navigateur
via "http://localhost:8080/config/test.php" si vous avez les réglages par défaut
de PhpMyAdmin)

setup.php va créer la base de données
test.php est optionnel, il va générer 500 utilisateurs pour pouvoir directement 
tester le site.
Le premier utilisateur est "test", les autres ont tous un numéro de 2 à 500.
Leur mot de passe pour tous est "Test17".
ex : id :           mdp :
    test            Test17
    test2           Test17
    test14          Test17
    test326         Test17
    
Il y a 3 "humains" et donc 497 androids, il y a 13 androids différents, il peut 
très bien y avoir 40 Androids ayant le même profil car ceux-ci sont, dans 
l'histoire du jeu, produits en série.

Pour ce qui est du CODE :
Le site est codé en PHP/SQL/HTML/CSS en très grande partie.
Deux parties doivent être en temps réel à 10 secondes près et ont donc été codées
en Ajax : les notifications et le système de Messagerie.
J'ai défini le temps de rafraichissement toutes les 5 secondes mais à 3 secondes
cela marchait très bien également.

Vous pouvez ensuite aller sur "http://localhost:8080/pages/login.php" et tester 
le site.