<?php
require "header.php";
require "../config/connect.php";
?>
   <body>
<div >
<center><h1 style="text-align:center" class="title">Profile</h1></center>
</div>
<?php if (isset($_SESSION['uidUser']))
{
	require "connect_header.php"?>
<?php require "menubox.php"?>
</div>
            <!-- MIDDLE-CONTAINER -->
            <div class="middle-container container">
		<div class="profile block" style="text-align:center"> <!-- PROFILE (MIDDLE-CONTAINER) -->
<form action="../manage_db/moreprofile.inc.php" method="post">
<h2 class="titular">Gender:</h2>
<div>
  <input type="checkbox" id="man" name="man"
         checked>
  <label for="man">Man</label>
   <img width="20px" alt="circle" src="../imgs/circle.png" >
  <input type="checkbox" id="lady" name="lady">
  <label for="lady">Lady</label>
</div>
<div>
  <input type="checkbox" id="android" name="android">
  <label for="android">Android</label>
   <img width="20px" alt="circle" src="../imgs/circle.png" >
  <input type="checkbox" id="other" name="other">
  <label for="other">Other</label>
</div>
<h2 class="titular">Attracted to :</h2>
<div>
  <input type="checkbox" id="man2" name="man2"
         checked>
  <label for="man2">Man</label>
   <img width="20px" alt="circle" src="../imgs/circle.png" >
  <input type="checkbox" id="lady2" name="lady2">
  <label for="lady2">Lady</label>
</div>
<div>
  <input type="checkbox" id="android2" name="android2">
  <label for="android2">Android</label>
   <img width="20px" alt="circle" src="../imgs/circle.png" >
  <input type="checkbox" id="other2" name="other2">
  <label for="other2">Other</label>
</div>
<div style="text-align:center">
  <center><button class="subscribe button" type="submit" name="moreprofile-submit">Save</button>
</div>
		</form>
<?php
if (isset($_GET['error']))
{
	        echo "<br/>";
	if ($_GET['error'] == "emptyfields")
		echo '<center><p class="error-msg">You need to chose</p></center>';
}
else if (isset($_GET['success']))
{
	echo "<br/>";
	if ($_GET['success'] == "changeok")
		echo '<center><p class="success-msg">Gender/Interests changed</p></center>';
	        if ($_GET['success'] == "biook")
			echo '<center><p class="success-msg">Bio changed</p></center>';
		        if ($_GET['success'] == "emptyok")
                echo '<center><p class="success-msg">Default Bio saved</p></center>';
}?>
                    </div>
                    <ul class="profile-options horizontal-list">
                    </ul>
                </div>

            <!-- RIGHT-CONTAINER -->
            <div class="right-container container">
                <div class="join-newsletter block" style="text-align:center; height:500px">
 <!-- INTERESTS (RIGHT-CONTAINER) -->
		    <h2 class="titular">BIOGRAPHY</h2>
<?php
	$bdd = connect();
	$idusr = $_SESSION['idUser'];
	$sql = "SELECT Bio FROM moreusers WHERE idUser = '$idusr'";
	$req = $bdd->prepare($sql);
	$req->execute();
	$req->bindColumn(1, $oldbio);
	$req->fetch(); ?>
		    <div class="input-container">
<form action="../manage_db/moreprofile.inc.php" method="post">
<textarea class="champ" name="bio" style="width:60%; height:300px; overflow:visible;"> <?php echo $oldbio; ?></textarea>
                        <div class="input-icon envelope-icon-newsletter"><span class="fontawesome-envelope scnd-font-color"></span></div>
		    <button class="subscribe button" name="bio-submit">Update</button>
</form>
</div>
                </div>
<!-- OK HERE -->
</div>
<?php
}
else /*IF NOT LOGIN*/
{
?>
<div class="card">
<?php
        echo    '<p style="text-align:center; color:black;">You first need to Login or Register to access this page</p>';
}
?>
</body>
