<?php
require "header.php";
require "../config/connect.php";
require "../manage_db/city.inc.php";
?>
   <body>
<div >
<center><h1 style="text-align:center" class="title">Search</h1></center>
</div>
<?php if (isset($_SESSION['uidUser']))
{
?>
<?php require "connect_header.php";?>
<div style="display : flex; justify-content:space-evenly;">
<?php //FILTER AGES ?>
<form method="post" action="#" class="sort-articles">
Looking for.. Age:
<select name="ages" class="mt-cate">
<option value="..">..</option>
<option value="0-2">0-2</option>
<option value="3-5">3-5</option>
<option value="6-10">6-10</option>
<option value="11-20">11-20</option>
<option value="21-30">21-30</option>
<option value="31-40">31-40</option>
<option value="41-60">41-60</option>
<option value="61+">61+</option>
</select>
<?php //FILTER DISTANCE ?>
Distance:
<select name="kilometers" class="mt-cate">
<option value="..">..</option>
<option value="0-10">0-10 Kms</option>
<option value="11-30">11-30 Kms</option>
<option value="31-50">31-50 Kms</option>
<option value="51-100">51-100 Kms</option>
<option value="101-200">101-200 Kms</option>
<option value="201-500">200-500 kms</option>
<option value="501+">501+ Kms</option>
</select>
<?php //FILTER POPU ?>
Popularity:
<select name="popular" class="mt-cate">
<option value="..">..</option>
<option value="0-2">0-2</option>
<option value="3-9">3-9</option>
<option value="10-20">10-20</option>
<option value="21-50">21-50</option>
<option value="51-100">51-100</option>
<option value="101-200">101-200</option>
<option value="201+">201+</option>
</select>
<?php //FILTER TAGS ?>
Tags:
<select name="hashtag" class="mt-cate">
<option value="..">..</option>
<option value="#Sports">#Sports</option>
<option value="#Blogging">#Blogging</option>
<option value="#Volunteering">#Volunteering</option>
<option value="#ArtAndDesign">#ArtAndDesign</option>
<option value="#Music">#Music</option>
<option value="#Reading">#Reading</option>
<option value="#VideoGaming">#VideoGaming</option>
<option value="#StrategicGaming">#StrategicGaming</option>
<option value="#Cleaning">#Cleaning</option>
<option value="#LickingThings">#LickingThings</option>
<option value="#RA9">#RA9</option>
<option value="#EdenClub">#EdenClub</option>
</select>
<button class="btn btn-secondary btn-sm mb-1" style="background-color:#354A67" type="submit" name="search-submit">Search</button>
</form>
</div>
</br>
	    <!-- MIDDLE-CONTAINER -->
	    <div class="middle-container container" style="max-width: 95%; width:95%;">
	    <div class="block" style="display:flex;flex-wrap:wrap;justify-content:space-evenly;" > <!-- PROFILE (MIDDLE-CONTAINER) --> <?php
	$bdd = connect();

	if (isset($_POST['search-submit']))
	{
		$ages=$_POST['ages'];
		$kilometers=$_POST['kilometers'];
		$popular=$_POST['popular'];
		$tags=$_POST['hashtag'];
		
		if (($ages == "..") && ($kilometers == "..") && ($popular == "..") && ($tags == ".."))
		{
			echo "You need to select something";
			exit();
		}
		$idusr = $_SESSION['idUser'];

		$bdd = connect();
		$sql2= "SELECT latitude, longitude, gender, interest, tags, popularity,Updated FROM moreusers WHERE idUser ='$idusr'";
		$req = $bdd->prepare($sql2);
		$req->execute();
		$req->bindColumn(1, $latiusr);
		$req->bindColumn(2, $longiusr);
		$req->bindColumn(3, $genderusr);
		$req->bindColumn(4, $interestusr);
		$req->bindColumn(5, $tagsusr);
		$req->bindColumn(6, $popuusr);
		$req->bindColumn(7, $uptodate);
		$req->fetch();
		$req->closeCursor();
		// CHECK SEXUALITY
		$array = explode(',', $interestusr);
		foreach ($array as $values)
		{
			if($values != "")
			{
				$sql2= "SELECT idUser, interest FROM moreusers WHERE gender LIKE CONCAT('%', '$values', '%')";
				$req = $bdd->prepare($sql2);
				$req->execute();
				$req->bindColumn(1, $genderok);
				$req->bindColumn(2, $interestok);
				while ($row = $req->fetch(PDO::FETCH_BOUND)) {
					$sql3 = "INSERT INTO temporary (idUser, interest) VALUES ('$genderok', '$interestok')" ;
					$req3 = $bdd->prepare($sql3);
					$req3->execute();
					$req3->fetch();
					$req3->closeCursor();
				}
				unset($genderok);
			}
		}

		$array2 = explode(',', $genderusr);
		foreach ($array2 as $values2)
		{
			if($values2 != "")
			{
				$sql3= "SELECT idUser FROM temporary WHERE interest LIKE CONCAT('%', '$values2', '%')";
				$req2 = $bdd->prepare($sql3);
				$req2->execute();
				$req2->bindColumn(1, $interestedok);
				while ($row = $req2->fetch(PDO::FETCH_BOUND)) {
					$sql4 = "SELECT COUNT(*) FROM suggestions WHERE idUser='$interestedok'" ;
					$req3 = $bdd->prepare($sql4);
					$req3->execute();
					$req3->bindColumn(1, $countexist);
					$req3->fetch();
					$req3->closeCursor();
					if ($countexist == 0)
					{
						$sql3 = "INSERT INTO suggestions (idUser, interest) VALUES ('$interestedok', '1')" ;
						$req3 = $bdd->prepare($sql3);
						$req3->execute();
						$req3->fetch();
						$req3->closeCursor();
						unset($countexist);
					}
				}
				unset($interestedok);
			}
		}

		//DELETE OWN PROFILE
		$sql4 = "DELETE FROM suggestions WHERE idUser='$idusr'" ;
		$req3 = $bdd->prepare($sql4);
		$req3->execute();
		$req3->fetch();
		$req3->closeCursor();

		//SET DISTANCE and POPU
		$sql4= "SELECT idUser FROM suggestions";
		$req3 = $bdd->prepare($sql4);
		$req3->execute();
		$req3->bindColumn(1, $user);
		while ($row = $req3->fetch(PDO::FETCH_BOUND)) {
			$sql5 = "SELECT latitude, longitude, popularity, Age, tags FROM moreusers WHERE idUser='$user'" ;
			$req4 = $bdd->prepare($sql5);
			$req4->execute();
			$req4->bindColumn(1, $otlati);
			$req4->bindColumn(2, $otlongi);
			$req4->bindColumn(3, $otpopu);
			$req4->bindColumn(4, $setage);
			$req4->bindColumn(5, $settags);
			$req4->fetch();
			$req4->closeCursor();
			$distance = calc2city($latiusr, $longiusr, $otlati, $otlongi);
			$sql5 = "UPDATE suggestions SET distance='$distance', popu='$otpopu', age='$setage', tagsin='$settags' WHERE idUser='$user'" ;
			$req4 = $bdd->prepare($sql5);
			$req4->execute();
			$req4->fetch();
			$req4->closeCursor();
		}

		//UPDATE TAGS
		$sql6= "SELECT idUser FROM suggestions";
		$req6 = $bdd->prepare($sql6);
		$req6->execute();
		$req6->bindColumn(1, $user2);
		while ($row = $req6->fetch(PDO::FETCH_BOUND)) {
			$counttags2 = 0;
			$array3 = explode(',', $tagsusr);
			foreach ($array3 as $values3)
			{
				if($values3 != "")
				{
					$sql7= "SELECT COUNT(*) FROM moreusers WHERE tags LIKE CONCAT('%', '$values3', '%') AND idUser='$user2'";
					$req7 = $bdd->prepare($sql7);
					$req7->execute();
					$req7->bindColumn(1, $counttags);
					$req7->fetch();
					$counttags2 = $counttags2 +$counttags;
				}
			}
			if ($counttags2 != 0) {
				$sql8 = "UPDATE suggestions SET tags='$counttags2' WHERE idUser='$user2'" ;
				$req8 = $bdd->prepare($sql8);
				$req8->execute();
				$req8->fetch();
				$req8->closeCursor();
			}
			unset($counttags);
		}
		if($ages == "..")
			$useage = "";
		else
		{
			if ($ages == "61+")
			{
				$useage= "age>60";
			}
			else
			{
				$range = explode("-", $ages);
				$useage = "age BETWEEN $range[0] AND $range[1]";
			}

		}
		if($kilometers == "..")
			$usekilo = "";
		else
		{
			if ($kilometers == "501+")
			{
				if ($ages !== "..")
				{
					$usekilo= "AND distance>500";
				}
				else
					$usekilo= "distance>500";
			}
			else
			{
				$range2 = explode("-", $kilometers);
				if ($ages == "..")
				{
					$usekilo = "distance BETWEEN $range2[0] AND $range2[1]";
				}
				else
					$usekilo = " AND distance BETWEEN $range2[0] AND $range2[1]";
			}

		}
		if($popular == "..")
			$usepopu = "";
		else
		{
			if ($popular == "201+")
			{
				if ($ages == ".." && $kilometers == "..")
				{
					$usepopu= "popu>200";
				}
				else
					$usepopu= "AND popu>200";
			}
			else
			{
				$range3 = explode("-", $popular);
				if ($ages == ".." && $kilometers == "..")
				{
					$usepopu = "popu BETWEEN $range3[0] AND $range3[1]";
				}
				else
					$usepopu = " AND popu BETWEEN $range3[0] AND $range3[1]";
			}

		}
		if($tags == "..")
			$usetags = "";
		else
		{
			if ($ages == ".." && $kilometers == ".." && $popular == "..")
			{
				$usetags = "tagsin LIKE CONCAT('%', '$tags', '%') ";
			}
			else
				$usetags = " AND tagsin  LIKE CONCAT('%', '$tags', '%')";

		}
		$sql9= "SELECT idUser, distance FROM suggestions WHERE $useage $usekilo $usepopu $usetags LIMIT 50";
		$req9 = $bdd->prepare($sql9);
		$req9->execute();
		$res = $req9->fetchAll();
		foreach ($res as $value3)
		{
			$sql7= "SELECT login FROM users WHERE id=".$value3['idUser']."";
			$req7 = $bdd->prepare($sql7);
			$req7->execute();
			$req7->bindColumn(1, $reslogin);
			$req7->fetch();
			$req7->closeCursor();
			$sql7= "SELECT COUNT(*) FROM Blocked WHERE blockeur='$idusr' AND blocked=".$value3['idUser']."";
			$req7 = $bdd->prepare($sql7);
			$req7->execute();
			$req7->bindColumn(1, $blocked);
			$req7->fetch();
			$req7->closeCursor();
			if ($blocked != 0)
			{
				continue;
			}
			$sql7= "SELECT gender,isonline,lastonline FROM moreusers WHERE idUser=".$value3['idUser']."";
			$req7 = $bdd->prepare($sql7);
			$req7->execute();
			$req7->bindColumn(1, $resgender);
			$req7->bindColumn(2, $resonline);
			$req7->bindColumn(3, $reslast);
			$req7->fetch();
			$req7->closeCursor();
			$sql7= "SELECT image1 FROM images WHERE idUser=".$value3['idUser']."";
			$req7 = $bdd->prepare($sql7);
			$req7->execute();
			$req7->bindColumn(1, $respic);
			$req7->fetch();
			$req7->closeCursor();
?>
<div class="detailBox" style="width:30%; position:relative">
<div class="commentBox">
  <center><?php echo "<a href='/pages/oprofile.php?login=$reslogin'>"?><?php echo "<h3>$reslogin</h3>";?></a></center>
<div class="profile-picture big-profile-picture clear">
<?php echo "<img src='$respic' width='150px'>"; ?>
</div>
  <center><?php echo "<span>Gender: $resgender</span>";?></center>
<?php if($resonline == 1)
{?>
  <center><?php echo "<span style='color:green'>En ligne</span>";?></center>
<?php
}
else
{?>
  <center><?php echo "<span>Connected: $reslast</span>";?></center>
<?php
}
?>
</div>
</div>
<?php
		}
	}
?>
<?php

	$sql = "TRUNCATE TABLE temporary";
	$req = $bdd->prepare($sql);
	$req->execute();
	$req->fetch();
	$req->closeCursor();
	$sql = "TRUNCATE TABLE suggestions";
	$req = $bdd->prepare($sql);
	$req->execute();
	$req->fetch();
	$req->closeCursor();
}
else /*IF NOT LOGIN*/
{
?>
<div class="card">
<?php
	echo    '<p style="text-align:center; color:black;">You first need to Login or Register to access your profile</p>';
}
?>
</div>
			</div>
    </body>
