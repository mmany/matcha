<?php
require "header.php";
require "../config/connect.php";
?>
   <body>
<div >
<center><h1 style="text-align:center" class="title">Profile</h1></center>
</div>
<?php if (isset($_SESSION['uidUser']))
{
?>
<?php require "connect_header.php"?>
<?php require "menubox.php"?>
</div>
	    <!-- MIDDLE-CONTAINER -->
	    <div class="middle-container container">
		<div class="profile block" style="text-align:center; width:150%"> <!-- PROFILE (MIDDLE-CONTAINER) -->
<form action="../manage_db/moreprofile.inc.php" method="post">
<h2 class="titular">Interests:</h2>
<div>
  <input type="checkbox" id="sports" name="sports">
  <label for="sports">#Sports</label>
   <img width="20px" alt="circle" src="../imgs/circle.png" >
  <input type="checkbox" id="blogging" name="blogging">
  <label for="blogging">#Blogging</label>
</div>
<div>
  <input type="checkbox" id="volunteering" name="volunteering">
  <label for="volunteering">#Volunteering</label>
   <img width="20px" alt="circle" src="../imgs/circle.png" >
  <input type="checkbox" id="art" name="art">
  <label for="art">#ArtAndDesign</label>
</div>
<div>
  <input type="checkbox" id="music" name="music">
  <label for="music">#Music</label>
   <img width="20px" alt="circle" src="../imgs/circle.png" >
  <input type="checkbox" id="reading" name="reading">
  <label for="reading">#Reading</label>
</div>
<div>
  <input type="checkbox" id="vgaming" name="vgaming">
  <label for="vgaming">#VideoGaming</label>
   <img width="20px" alt="circle" src="../imgs/circle.png" >
  <input type="checkbox" id="sgaming" name="sgaming">
  <label for="sgaming">#StrategicGaming</label>
</div>
<div>
  <input type="checkbox" id="cleaning" name="cleaning">
  <label for="cleaning">#Cleaning</label>
   <img width="20px" alt="circle" src="../imgs/circle.png" >
  <input type="checkbox" id="licking" name="licking">
  <label for="licking">#LickingThings</label>
</div>
<div>
  <input type="checkbox" id="RA9" name="RA9"
         checked>
  <label for="RA9">#RA9</label>
   <img width="20px" alt="circle" src="../imgs/circle.png" >
  <input type="checkbox" id="eden" name="eden">
  <label for="eden">#EdenClub</label>
</div>
<div style="text-align:center">
  <center><button class="subscribe button" type="submit" name="interests-submit">Save</button>
</div>
		</form>
		    </div>
		    <ul class="profile-options horizontal-list">
		    </ul>
		</div>
<?php
}
else /*IF NOT LOGIN*/
{
?>
<div class="card">
<?php
	echo    '<p style="text-align:center; color:black;">You first need to Login or Register to access this page</p>';
}
?>
</div>
</body>
