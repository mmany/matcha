<?php
require "header.php";
require "../config/connect.php";
?>
   <body>
<div >
<center><h1 style="text-align:center" class="title">Profile</h1></center>
</div>
<?php if (isset($_SESSION['uidUser']))
{
?>
<?php require "connect_header.php"?>
<?php require "menubox.php"?>

<!-- POPULARITY SCORE -->

		<div class=" block" style="height=300px"> <!-- DONUT CHART BLOCK (LEFT-CONTAINER) -->
		    <h2 class="titular" style="background: #546B8B;">POPULARITY SCORE</h2>
		    <div class="donut-chart">
<?php
	$bdd = connect();

	//MAJ POPULARITY
	$sql4 = "SELECT COUNT(*) FROM managelike WHERE idUid ='$idusr' AND IsVisit != 0";
	$q4 = $bdd->prepare($sql4);
	$q4->execute();
	$q4->bindColumn(1, $nbvisit);
	$q4->fetch();
	$q4->closeCursor();
	$sql4 = "SELECT COUNT(*) FROM managelike WHERE idUid ='$idusr' AND IsLike != 0";
        $q4 = $bdd->prepare($sql4);
        $q4->execute();
        $q4->bindColumn(1, $nblikes);
        $q4->fetch();
	$q4->closeCursor();
	$sql4 = "SELECT COUNT(*) FROM managelike WHERE idUid ='$idusr' AND IsMatcha != 0";
        $q4 = $bdd->prepare($sql4);
        $q4->execute();
        $q4->bindColumn(1, $nbmatch);
        $q4->fetch();
	$q4->closeCursor();
	$res = $nbvisit + ($nblikes * 2) + ($nbmatch * 3);

	$sql = "UPDATE moreusers SET popularity='$res' WHERE idUser = '$idusr'";
		$req3 = $bdd->prepare($sql);
		$req3->execute();
		$req3->fetch();
		$req3->closeCursor();

	$sql = "SELECT popularity,Age,localisation FROM moreusers WHERE idUser = '$idusr'";
        $req = $bdd->prepare($sql);
        $req->execute();
        $req->bindColumn(1, $popu);
        $req->bindColumn(2, $age);
        $req->bindColumn(3, $loca);
        $req->fetch();
?>
	<p class="center-date" style="text-align:center"><?php echo $popu;?></p>
<!-- inserer popularity score here-->
		    </div>
</div>
</div>
	    <!-- MIDDLE-CONTAINER -->
	    <div class="middle-container container">
		<div class="block"> <!-- PROFILE (MIDDLE-CONTAINER) -->
		    <a class="add-button" href="moreprofile.php"><span class="icon entypo-plus scnd-font-color">+</span></a>
		    <div class="profile-picture big-profile-picture clear">
		    <img width="150px" alt="Connor picture" src="<?php echo $ppic;?>" >
		    </div>
<?php
        $bdd = connect();
        $sql = "SELECT firstname,name FROM users WHERE id = '$idusr'";
        $req = $bdd->prepare($sql);
        $req->execute();
        $req->bindColumn(1, $fname);
        $req->bindColumn(2, $lname);
        $req->fetch(); ?>
		    <h1 class="user-name"><?php echo "$uidusr"; ?></h1>
                    <h1 class="user-name" style="font-size:14px;overflow:hidden;word-wrap: break-word;"><?php echo "($fname $lname, $age y.o., $loca)"; ?></h1>
		    <div class="profile-description">
		    <h1 class="user-name" style="font-size:14px;overflow:hidden;word-wrap: break-word;"><?php echo "Gender: "; ?></h1>
<?php
        $sql2 = "SELECT gender FROM moreusers WHERE idUser = '$idusr'";
        $req2 = $bdd->prepare($sql2);
        $req2->execute();
        $req2->bindColumn(1, $gend);
        $req2->fetch();
        $array = explode(',', $gend);
        foreach ($array as $values)
        {?>
        <span class="scnd-font-color" style="overflow:hidden;word-wrap: break-word;"><?php echo "$values ";?></span><?php
        }
?>
		    <h1 class="user-name" style="font-size:14px;overflow:hidden;word-wrap: break-word;"><?php echo "Interested in: "; ?></h1>
<?php
	$sql2 = "SELECT interest FROM moreusers WHERE idUser = '$idusr'";
        $req2 = $bdd->prepare($sql2);
        $req2->execute();
        $req2->bindColumn(1, $inte);
        $req2->fetch();
        $array = explode(',', $inte);
        foreach ($array as $values)
        {?>
        <span class="scnd-font-color" style="overflow:hidden;word-wrap: break-word;"><?php echo "$values ";?></span><?php
        }
?>
<?php
	$bdd = connect();
	$sql = "SELECT Bio FROM moreusers WHERE idUser = '$idusr'";
	$req = $bdd->prepare($sql);
	$req->execute();
	$req->bindColumn(1, $oldbio);
	$req->fetch(); ?>
                    <h1 class="user-name" style="font-size:14px;overflow:hidden;word-wrap: break-word;"><?php echo "Biography: "; ?></h1>
	<p class="scnd-font-color"><?php echo $oldbio;?></p>
		    </div>
		    <ul class="profile-options horizontal-list">
		    </ul>
		</div>

</div>
	    <!-- RIGHT-CONTAINER -->
	    <div class="right-container container">
		<div class="join-newsletter block" style="text-align:center; height:280px">
 <!-- INTERESTS (RIGHT-CONTAINER) -->
		    <h2 class="titular" style="background: #546B8B;">INTERESTS</h2>
		    <div class="input-container">
<?php
	$sql2 = "SELECT tags FROM moreusers WHERE idUser = '$idusr'";
	$req2 = $bdd->prepare($sql2);
	$req2->execute();
	$req2->bindColumn(1, $tags);
	$req2->fetch();
	$array = explode(',', $tags);
	foreach ($array as $values)
	{?>
	<span class="scnd-font-color" style="overflow:hidden;word-wrap: break-word;"><?php echo "$values ";?></span><?php
	}
?>
	<p class="scnd-font-color" style="overflow:hidden;word-wrap: break-word;"><?php ;?></p>
			<div class="input-icon envelope-icon-newsletter"><span class="fontawesome-envelope scnd-font-color"></span></div>
		    </div>
		    <a class="subscribe button" href="interest.php">Change Interest</a>
		</div>
<!-- OK HERE -->
		<div class="block"> <!-- PICTURES (RIGHT-CONTAINER) -->
		    <h2 class="titular" style="background: #546B8B;">PICTURES</h2>
		    <div class="input-container">
<div class="d-flex justify-content-auto">
<?php
        $sql2 = "SELECT image1,image2,image3,image4,image5 FROM images WHERE idUser = '$idusr'";
        $req2 = $bdd->prepare($sql2);
        $req2->execute();
	$req2->bindColumn(1, $pic1);
	$req2->bindColumn(2, $pic2);
	$req2->bindColumn(3, $pic3);
	$req2->bindColumn(4, $pic4);
	$req2->bindColumn(5, $pic5);
	$req2->fetch(); ?>
		    <div class="profile-picture big-profile-picture clear" style="width:100px; height:100px;float:left;">
			    <img width="100px" alt="Connor picture" src="<?php echo $pic1;?>" >
			</div>
		    <div class="profile-picture big-profile-picture clear" style="width:100px; height:100px;">
		    <img width="100px" alt="Connor picture" src="<?php echo $pic2;?>" >
			</div>
</div>
<div class="d-flex justify-content-auto">
                    <div class="profile-picture big-profile-picture clear" style="width:100px; height:100px;float:left;">
		    <img width="100px" alt="Connor picture" src="<?php echo $pic3;?>" >
                        </div>
                    <div class="profile-picture big-profile-picture clear" style="width:100px; height:100px;">
		    <img width="100px" alt="Connor picture" src="<?php echo $pic4;?>" >
                        </div>
</div>
		    <div class="profile-picture big-profile-picture clear" style="width:100px; height:100px;">
		    <img width="100px" alt="Connor picture" src="<?php echo $pic5;?>" >
			</div>
		    </div>
		    <div class="input-container">
		    </div>
	    </div> <!-- end right-container -->
	</div> <!-- end main-container -->
<?php
}
else /*IF NOT LOGIN*/
{
?>
<div class="card">
<?php
	echo    '<p style="text-align:center; color:black;">You first need to Login or Register to access your profile</p>';
}
?>
			</div>
    </body>
