<?php
require "header.php";
?>
<main>
<div >
<center><h1 style="text-align:center" class="title">Register</h1></center>
</div>
<?php
if (isset($_SESSION['uidUser']))
{
	header("Location: profile.php");
}
?>
<div class="card">
<?php
if (isset($_GET['error']))
{
	echo '<div style="text-align:center">';
	if ($_GET['error'] == "emptyfields")
		echo '<p class="error-msg">You need to fill in all the fields</p>';
	if ($_GET['error'] == "invalidname")
		echo '<p class="error-msg">Invalid First Name and/or Last Name</p>';
	if ($_GET['error'] == "invalidmail")
		echo '<p class="error-msg">Invalid email</p>';
	if ($_GET['error'] == "emailtaken")
		echo '<p class="error-msg">This email already exists</p>';
	if ($_GET['error'] == "invaliduid")
		echo '<p class="error-msg">Invalid username</p>';
	if ($_GET['error'] == "invalidpassword")
		echo '<p class="error-msg">Password must be at least 6 characters long and must contain one number, one lowercase and one uppercase character</p>';
	if ($_GET['error'] == "passwordcheck")
		echo '<p class="error-msg">Passwords don\'t match</p>';
	if ($_GET['error'] == "usertaken")
		echo '<p class="error-msg">Username taken</p>';
}
else if (isset($_GET['register']))
{
	if ($_GET['register'] == "success")
		echo '<center><p class="success-msg">You registered successfully <br/> Please validate your email</p></center>';
}
?>
<div style="text-align:center">
		<form action="../manage_db/register.inc.php" method="post">
Username and E-mail: </br>
		    <input type="text" name="uid" class="champ" placeholder="Username">
		    <input type="text" name="mail" class="champ" placeholder="E-mail">
	   </br> </br>Name: </br><input type="text" name="fname" class="champ" placeholder="First Name">
<input type="text" name="lname" class="champ" placeholder="Last Name">
		   </br> </br>Password: </br><input type="password" name="pwd" class="champ" placeholder="Password">
		    <input type="password" name="pwd-repeat" class="champ" placeholder="Repeat password">
			<center><a href="#"><button class="btn3" type="submit" name="register-submit">Register</button></a>
			<a href="login.php"><div id="btn2">Sign in</div></a></center>
		</form>
</div>
	    </div>
	</main>
    </body>
</html>
