<?php
require "header.php";
require "../config/connect.php";
$idusr2 = $_SESSION['idUser'];
?>
   <body>
<div >
<center><h1 style="text-align:center" class="title">Profile</h1></center>
</div>
<?php if (isset($_SESSION['uidUser']))
{
	$bdd = connect();
        $sql2 = "SELECT image1,image2,image3,image4,image5 FROM images WHERE idUser = '$idusr2'";
        $req2 = $bdd->prepare($sql2);
        $req2->execute();
	$req2->bindColumn(1, $pic1);
	$req2->bindColumn(2, $pic2);
	$req2->bindColumn(3, $pic3);
	$req2->bindColumn(4, $pic4);
	$req2->bindColumn(5, $pic5);
	$req2->fetch();
?>
<?php require "connect_header.php"?>
<?php require "menubox.php"?>
</div>
            <!-- MIDDLE-CONTAINER -->
            <div class="middle-container container" style="width:400px">
		<div class="block" style="text-align:center;"> <!-- PROFILE (MIDDLE-CONTAINER) -->
<h2 class="titular" style="font-size:16px">Which picture would you like to change ?</h2>
                    <div class="input-container">
<div class="d-flex justify-content-auto">
                    <div class="profile-picture big-profile-picture clear" style="width:100px; height:100px;float:left;">
                        <a href="picture1.php"><img width="100px" alt="Connor picture" src="<?php echo $pic1;?>" ></a>
                        </div>
                    <div class="profile-picture big-profile-picture clear" style="width:100px; height:100px;">
                        <a href="picture2.php"><img width="100px" alt="Connor picture" src="<?php echo $pic2;?>" ></a>
                        </div>
</div>
<div class="d-flex justify-content-auto">
                    <div class="profile-picture big-profile-picture clear" style="width:100px; height:100px;float:left;">
                        <a href="picture3.php"><img width="100px" alt="Connor picture" src="<?php echo $pic3;?>" ></a>
                        </div>
                    <div class="profile-picture big-profile-picture clear" style="width:100px; height:100px;">
                        <a href="picture4.php"><img width="100px" alt="Connor picture" src="<?php echo $pic4;?>" ></a>
                        </div>
</div>
                    <div class="profile-picture big-profile-picture clear" style="width:100px; height:100px;">
                        <a href="picture5.php"><img width="100px" alt="Connor picture" src="<?php echo $pic5;?>" ></a>
                        </div>
                    </div>
                    <ul class="profile-options horizontal-list">
                    </ul>
		</div>
</div>
<?php
}
else /*IF NOT LOGIN*/
{
?>
<div class="card">
<?php
        echo    '<p style="text-align:center; color:black;">You first need to Login or Register to access this page</p>';
}
?>
</body>
