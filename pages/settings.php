<?php
require "header.php";
require "../config/connect.php";
?>
   <body>
<div >
<center><h1 style="text-align:center" class="title">Profile</h1></center>
</div>
<?php if (isset($_SESSION['uidUser']))
{
?>
<?php require "connect_header.php"?>
<?php require "menubox.php"?>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDvS6ZGaIevhuaSxWVlOxXAexTdr9yykuQ&libraries=places" type="text/javascript"></script>
	<script type="text/javascript">
	function initialize() {

		var options = {
		types: ['(cities)'],
	};

		var input = document.getElementById('recherche-ville');
		var autocomplete = new google.maps.places.Autocomplete(input, options);
	}
	google.maps.event.addDomListener(window, 'load', initialize);
	</script>


	    <!-- MIDDLE-CONTAINER -->
	    <div class="middle-container container" style="width:600px">
		<div class="block" style="text-align:center"> <!-- PROFILE (MIDDLE-CONTAINER) -->
<?php
	if (isset($_GET['error']))
	{
		echo '<div style="text-align:center">';
		if ($_GET['error'] == "emptyfields")
			echo '<p class="error-msg">You need to fill in the fields</p>';
		if ($_GET['error'] == "invalidname")
			echo '<p class="error-msg">Invalid First Name and/or Last Name</p>';
		if ($_GET['error'] == "invalidmail")
			echo '<p class="error-msg">Invalid email</p>';
		if ($_GET['error'] == "emailtaken")
			echo '<p class="error-msg">This email already exists</p>';
		if ($_GET['error'] == "invaliduid")
			echo '<p class="error-msg">Invalid username</p>';
		if ($_GET['error'] == "invalidpassword")
			echo '<p class="error-msg">Password must be at least 6 characters long and must contain one number, one lowercase and one uppercase character</p>';
		if ($_GET['error'] == "passwordcheck")
			echo '<p class="error-msg">Passwords don\'t match</p>';
		if ($_GET['error'] == "usertaken")
			echo '<p class="error-msg">Username taken</p>';
		if ($_GET['error'] == "invalid")
			echo '<p class="error-msg">Invalid City</p>';
		if ($_GET['error'] == "invalidage")
			echo '<p class="error-msg">Age not correct</p>';
	}
	else if (isset($_GET['success']))
	{
		if ($_GET['success'] == "nameok")
			echo '</br><center><p class="success-msg">Name changed !</p></center>';
		if ($_GET['success'] == "mailok")
			echo '</br><center><p class="success-msg">Mail changed !</p></center>';
		if ($_GET['success'] == "passwordoks")
			echo '</br><center><p class="success-msg">Passwords changed !</p></center>';
		if ($_GET['success'] == "localok")
			echo '</br><center><p class="success-msg">City changed !</p></center>';
		if ($_GET['success'] == "ageok")
			echo '</br><center><p class="success-msg">Age changed !</p></center>';
	}
?>

<h2 class="titular">Change your name:</h2>
<div>
<form class="form-inline" action="../manage_db/settings.inc.php" method="post">
</br><input type="text" name="fname" class="champ" placeholder="First Name">
<input type="text" name="lname" class="champ" placeholder="Last Name">
<center><a href="#"><button class="btn4" type="submit" name="setname-submit">Save</button></a>
</form>
</div>
   <img width="50px" alt="circle" src="../imgs/circle.png" >
<h2 class="titular">Change your email adress:</h2>
<div>
<form class="form-inline" action="../manage_db/settings.inc.php" method="post">
</br><input type="text" name="mail" class="champ" placeholder="E-mail">
<center><a href="#"><button class="btn4" type="submit" name="setmail-submit">Save</button></a>
</form>
</div>
   <img width="50px" alt="circle" src="../imgs/circle.png" >
<h2 class="titular">Change your password:</h2>
<div>
<form class="form-inline" action="../manage_db/settings.inc.php" method="post">
</br><input type="password" name="pwd" class="champ" placeholder="Password">
<input type="password" name="pwd-repeat" class="champ" placeholder="Repeat password">
<center><a href="#"><button class="btn4" type="submit" name="setpass-submit">Save</button></a>
</form>
</div>
   <img width="50px" alt="circle" src="../imgs/circle.png" >
<h2 class="titular">Change your localisation:</h2>
<div>
<form class="form-inline" action="../manage_db/settings.inc.php" method="post">
</br><select class="champ" name="local">
  <option value="Paris">Paris</option>}
  <option value="Marseille">Marseille</option>
  <option value="Lyon">Lyon</option>
  <option value="Toulouse">Toulouse</option>
  <option value="Nice">Nice</option>
  <option value="Nantes">Nantes</option>
  <option value="Strasbourg">Strasbourg</option>
  <option value="Montpellier">Montpellier</option>
  <option value="Bordeaux">Bordeaux</option>
  <option value="Lilles">Lille</option>}
  <option value="Rennes">Rennes</option>
  <option value="Reims">Reims</option>
  <option value="Philadelphia">Philadelphia</option>
  <option value="Florida">Florida</option>
  <option value="Los Angeles">Los Angeles</option>
  <option value="Miami">Miami</option>
  <option value="Madrid">Madrid</option>
  <option value="Barcelona">Barcelona</option>
  <option value="Berlin">Berlin</option>}
  <option value="Stockholm">Stockholm</option>
  <option value="Helsinki">Helsinki</option>
  <option value="Nairobi">Nairobi</option>
  <option value="Denver">Denver</option>
  <option value="Tokyo">Tokyo</option>
  <option value="Rio">Rio</option>
  <option value="Moscou">Moscou</option>
  <option value="Oslo">Oslo</option>
  <option value="Palerme">Palerme</option>}
  <option value="London">London</option>
  <option value="Reykjavik">Reykjavik</option>}
  <option value="Chicago">Chicago</option>
  <option value="San Francisco">San Francisco</option>
  <option value="Seoul">Seoul</option>
  <option value="Mexico City">Mexico City</option>
  <option value="Mumbai">Mumbai</option>
  <option value="Detroit">Detroit</option>
</select>
<center><a href="#"><button class="btn4" type="submit" name="setlocal-submit">Save</button></a>
</form>
</div>
   <img width="50px" alt="circle" src="../imgs/circle.png" >
<h2 class="titular">Change your age:</h2>
<div>
<form class="form-inline" action="../manage_db/settings.inc.php" method="post">
</br><input type="text" name="age" class="champ" placeholder="Age">
<center><a href="#"><button class="btn4" type="submit" name="setage-submit">Save</button></a>
</form>
</div>
 <ul class="profile-options horizontal-list">
		    </ul>
		</div>
</div>
<?php
}
else /*IF NOT LOGIN*/
{
?>
<div class="card">
<?php
	echo    '<p style="text-align:center; color:black;">You first need to Login or Register to access this page</p>';
}
?>
</body>
