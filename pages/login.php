<?php
require "header.php";
?>
<main>
<div >
<center><h1 style="text-align:center" class="title">Login</h1></center>
</div>
<?php
if (isset($_SESSION['uidUser']))
{
	header("Location: profile.php");
}
?>
<div class="card">
<?php
if (isset($_GET['error']))
{
	echo '<div style="text-align:center">';
	if ($_GET['error'] == "emptyfields")
		echo            '<p class="error-msg">You need to fill all the fields</p>';
	if ($_GET['error'] == "wrongpwd")
		echo            '<p class="error-msg">Either the username or the password is wrong</p>';
	if ($_GET['error'] == "noactivated")
		echo            '<p class="error-msg">You need to activate your e-mail before</p>';
	if ($_GET['error'] == "nouser")
		echo            '<p class="error-msg">This user does not exist</p>';
	echo '</div>';
}
else if (isset($_GET['login']))
{
	if ($_GET['login'] == "success")
		echo '<center><p class="success-msg">You are connected !</p></center>';
}
?>
<div style="text-align:center">
<form class="form-inline" action="../manage_db/login.inc.php" method="post">
Username: <input class="champ" type="text" name="uid" placeholder="Username">
</br>Password: <input class="champ" type="password" name="pwd" placeholder="Password">
	<div style="text-align:center">
	<center><button class="btn4" type="submit" name="login-submit">Login</button></center>
	</div>
	</form>
</div>
<a href="forgotPwd.php" style="text-align:right; font-size:12px; color:#354A67">Forgot your password ?<a>
	</div>
	</main>
</body>
</html>
