<?php
require "../pages/header.php";
?>
		<main>
<div >
<center><h1 style="text-align:center" class="title">Login</h1></center>
</div>
<div class="card">
<?php
if (isset($_GET["username"]) && isset($_GET["keyf"]))
{
	$_SESSION["uidUsers"] = $_GET["username"];
	$_SESSION["keyf"] = $_GET["keyf"];
}
if (isset($_GET['error']))
{
    echo '<div style="text-align:center">';
    if ($_GET['error'] == "emptyfields")
        echo '<p class="error-msg">You need to fill in all the fields</p>';
        if ($_GET['error'] == "keyinvalid")
        echo '<p class="error-msg">Your Key is a wrong one, please try again from the beginning</p>';
    //password
    if ($_GET['error'] == "invalidpassword")
        echo '<p class="error-msg">Password must be at least 6 characters and contain one number, one lower case and one upper case character</p>';
    if ($_GET['error'] == "passwordcheck")
		echo '<p class="error-msg">Passwords don\'t match</p>';
	echo "</div>";
}
else if (isset($_GET['change']))
{
	if ($_GET['change'] == "passwordsuccess")
	{
		echo '<center><p class="success-msg">You have changed your password successfully</p></center>';
	}
}
?>
                <h3 style="text-align:center">Change Password</h3>
				<hr />
                <form class="form-inline" action="../manage_db/changeagain.inc.php" method="post">
                    <input class="form-control col-md-4" style="left:15%" type="password" name="newpass" placeholder="New Password">
                    <input class="form-control col-md-4" style="left:15%" type="password" name="newpass2" placeholder="Repeat Password">
                    <div style="text-align:center">
                        <button class="btn" type="submit" name="modifypwdagain-submit">Change Password</button>
                    </div>
                </form><br>
            </div>
        </main>
    </body>
</html>
