<link rel="stylesheet" href="../css/notif.css">
<?php
$uidusr = $_SESSION['uidUser'];
$idusr = $_SESSION['idUser'];

$bdd = connect();
$sql2 = "SELECT Profile FROM images WHERE idUser = '$idusr'";
$req2 = $bdd->prepare($sql2);
$req2->execute();
$req2->bindColumn(1, $profile);
$req2->fetch();
$sql2 = "SELECT $profile FROM images WHERE idUser = '$idusr'";
$req2 = $bdd->prepare($sql2);
$req2->execute();
$req2->bindColumn(1, $ppic);
$req2->fetch();?>
<script>
$(document).ready(function(){

	// updating the view with notifications using ajax
	function load_unseen_notification(view = '')
	{
		$.ajax({
		  url:"../manage_db/fetchnotif.php",
			  method:"POST",
			  data:{view:view},
			  dataType:"json",
			  success:function(data)
				  {
				$('.notil').html(data.notification);
				if(data.unseen_notification > 0)
				{
					$('.count').html(data.unseen_notification);
				}
			}
	});
	}
	load_unseen_notification('yes');

	$(document).on('click', '.noti', function(){
		$('.count').html('');
		load_unseen_notification('seen');
	});
	setInterval(function(){
		$('.count').html('');
		load_unseen_notification('yes');
	},5000);
});
</script>
		<div class="main-container">

	    <!-- HEADER BAR -->
	    <header class="block">
		<ul class="header-menu horizontal-list">
		    <li>
			<a class="header-menu-tab" href="suggestions.php"><span class="icon entypo-cog scnd-font-color"></span>Suggestions</a>
		    </li>
		    <li>
			<a class="header-menu-tab" href="search.php"><span class="icon fontawesome-user scnd-font-color"></span>Search</a>
		    </li>
		    <li>
			<a class="header-menu-tab" href="message.php"><span class="icon fontawesome-envelope scnd-font-color"></span>Messages</a>
		    </li>
		    <li>
			<a class="header-menu-tab" href="matchs.php"><span class="icon fontawesome-star-empty scnd-font-color"></span>Matchs</a>
		    </li>
		    <li>
			<a class="header-menu-tab" href="likes.php"><span class="icon fontawesome-star-empty scnd-font-color"></span>Likes</a>
		    </li>
		    <li>
			<a class="header-menu-tab" href="visits.php"><span class="icon fontawesome-star-empty scnd-font-color"></span>Visitors</a>
		    </li>
<div style="line-height:65px">
		  <li ><a href="notification.php" style="position: relative; top: 6px; left:2px;"><span class="label label-pill label-danger count" style="border-radius:10px; color: red;"></span> <i class="fa fa-bell" aria-hidden="true" style="color: #9099b7;"></i></a>
		</li></div>
		</ul>
		<div class="profile-menu">
		    <p><a href="profile.php"><span class="entypo-down-open scnd-font-color">Me</span></a></p>
		    <div class="profile-picture small-profile-picture">
		    <img width="40px" alt="profile picture" src="<?php echo $ppic;?>">
		    </div>
		</div>
</header>
