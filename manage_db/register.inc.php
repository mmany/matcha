<?php
require "../pages/header.php";
require "mailActivation.php";
require "../config/connect.php";
require "city.inc.php";

if (isset($_POST['register-submit']))
{
	$mail = $_POST['mail'];
	$username = $_POST['uid'];
	$fname = $_POST['fname'];
	$lname = $_POST['lname'];
	$password = $_POST['pwd'];
	$password2 = $_POST['pwd-repeat'];

	if (empty($username) || empty($mail) || empty($password) || empty($password2) || empty($fname) || empty($lname))
	{
		header("Location: ../pages/register.php?error=emptyfields");
		exit();
	}
	else if (!preg_match("/^[a-zA-Z0-9]*$/", $username))
	{
		header("Location: ../pages/register.php?error=invaliduid");
		exit();
	}
	else if (!preg_match("/^[a-zA-Z0-9]*$/", $fname) && !preg_match("/^[a-zA-Z0-9]*$/", $lname))
	{
		header("Location: ../pages/register.php?error=invalidname");
		exit();
	}
	else if (!filter_var($mail, FILTER_VALIDATE_EMAIL))
	{
		header("Location: ../pages/register.php?error=invalidmail");
		exit();
	}
	else if (!preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,}$#', $password))
	{
		header("Location: ../pages/register.php?error=invalidpassword");
		exit();
	}
	else if ($password !== $password2)
	{
		header("Location: ../pages/register.php?error=passwordcheck");
		exit();
	}
	else
	{
		/*CHECK IF NAME TAKEN*/
		$bdd = connect();
		$sql = "SELECT login FROM users WHERE login= :username";
		$req = $bdd->prepare($sql);
		$req->execute(['username' => $username]);
		if($checkname = $req->fetch()) //check if a line contain the same name
		{
			header("Location: ../pages/register.php?error=usertaken");
			$req->closeCursor();
			exit();
		}
		/*CHECK IF EMAIL TAKEN*/
		$sql = "SELECT email FROM users WHERE email= :mail";
		$req = $bdd->prepare($sql);
		$req->execute(['mail' => $mail]);
		if($checkname = $req->fetch()) //check if a line contain the same email
		{
			header("Location: ../pages/register.php?error=emailtaken");
			$req->closeCursor();
			exit();
		}
		else
		{   
			$keyf = md5(microtime(TRUE)*100000);
			activateMail($username, $mail, $keyf);
			$sql = "INSERT INTO users (firstname, name, login, email, password, keyf) VALUES (:fname, :lname, :username, :mail, :pwd, :keyf)";
			$hashedPwd = password_hash($password, PASSWORD_DEFAULT);
			$req = $bdd->prepare($sql);
			$req->execute(['fname' => $fname, 'lname' => $lname, 'username' => $username, 'mail' => $mail, 'pwd' => $hashedPwd, 'keyf' => $keyf]);
			$req->closeCursor();
			$sql = "SELECT id FROM users WHERE login = '$username'";
			$req3 = $bdd->prepare($sql);
			$req3->execute();
			$req3->bindColumn(1, $recup);
			$req3->fetch();

			$ip = shell_exec("curl ifconfig.me");
			$looking = "city";
			$city = getgeoloc($ip, "city");
			$country = getgeoloc2($ip, "country");
			$city = addslashes($city);
			$coord = getposbycity($city, $country);
			$sql = "INSERT INTO moreusers (idUser, gender, interest, localisation, country, latitude, longitude) VALUES ('$recup', 'android', 'man,lady,android', '$city', '$country', '$coord[lat]', '$coord[long]')";
			$req2 = $bdd->prepare($sql);
			$req2->execute();
			$sql = "INSERT INTO images (idUser) VALUES ('$recup')" ;
			$req2 = $bdd->prepare($sql);
			$req2->execute();
			mkdir("../imgs/profile/$username");
			header("Location: ../pages/register.php?register=success");
			exit();
		}
	}
}
?>

