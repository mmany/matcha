<?php

//Distance entre 2 villes
function calc2city($point1_lat, $point1_long, $point2_lat, $point2_long, $decimals = 2) {
    $degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));
    $distance = $degrees * 111.13384; // 1 degré = 111,13384 km, sur base du diamètre moyen de la Terre (12735 km)
    return round($distance, $decimals);
}


//Longitude et latitude d'une ville
function getposbycity($city, $country)
{
    $json = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=".$city."+".$country."&key=AIzaSyDvS6ZGaIevhuaSxWVlOxXAexTdr9yykuQ"), true);
    $result = array('lat' => $json['results'][0]['geometry']['location']['lat'],
                    'long' => $json['results'][0]['geometry']['location']['lng']);
    return($result);
}


//Avoir la ville depuis l'IP
function getgeoloc($ip, $what)
{
	$api = unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
    if($api && $api['status'] == 'success') 
    {
        return $api[$what];
    }
    else
	    return $api[$what] = "Detroit";
}

//Avoir le pays depuis l'IP
function getgeoloc2($ip, $what)
{
        $api = unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
    if($api && $api['status'] == 'success')
    {
        return $api[$what];
    }
    else
            return $api[$what] = "United-States";
}
?>
