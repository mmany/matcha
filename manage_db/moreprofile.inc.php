<?php
require "../pages/header.php";
require "mailActivation.php";
require "../config/connect.php";

if (isset($_POST['moreprofile-submit']))
{
	if (isset($_POST['man']))
		$man = $_POST['man'];
	else
		$man=3;
	if (isset($_POST['lady']))
		$lady = $_POST['lady'];
	else
		$lady=3;
	if (isset($_POST['android']))
		$android = $_POST['android'];
	else
		$android=3;
	if (isset($_POST['other']))
		$other = $_POST['other'];
	else
		$other=3;

	if (isset($_POST['man2']))
		$man2 = $_POST['man2'];
	else
		$man2=3;
	if (isset($_POST['lady2']))
		$lady2 = $_POST['lady2'];
	else
		$lady2=3;
	if (isset($_POST['android2']))
		$android2 = $_POST['android2'];
	else
		$android2=3;
	if (isset($_POST['other2']))
		$other2 = $_POST['other2'];
	else
		$other2=3;
	$idusr = $_SESSION['idUser'];
	$bdd = connect();
	$sql = "SELECT idUser FROM moreusers WHERE idUser = '$idusr'";
	$req = $bdd->prepare($sql);
	$req->execute();
	$req->bindColumn(1, $exist);
	$req->fetch();
	if (!isset($exist))
	{
		$sql = "INSERT INTO moreusers (idUser, gender, interest) VALUES ('$idusr', 'android', 'man,lady,android')" ;
		$req = $bdd->prepare($sql);
		$req->execute();
	}
	if (($man==3 && $other==3 && $lady==3 && $android==3) || ($man2==3 && $lady2==3 && $other2==3 && $android2==3))
	{
		$sql = "UPDATE moreusers SET 
			idUser = '$idusr',
			gender = 'android',
			interest = 'man,lady,android' WHERE idUser = '$idusr'" ;
		$req = $bdd->prepare($sql);
		$req->execute();
		header("Location: ../pages/moreprofile.php?error=emptyfields");
		$req->closeCursor();
		exit();
	}
	else
	{
		/* GENDER */
		$sql3= "UPDATE moreusers SET gender=',' WHERE idUser = '$idusr'";
		$req3 = $bdd->prepare($sql3);
		$req3->execute();
		$req3->fetch();
		$req3->closeCursor();
		if ($man != 3)
		{
			$sql2= "UPDATE moreusers SET gender=CONCAT(gender, 'man,' ) WHERE idUser = '$idusr'";
			$req2 = $bdd->prepare($sql2);
			$req2->execute();
			$req2->closeCursor();
		}
		if($lady != 3)
		{
			$sql4= "UPDATE moreusers SET gender=CONCAT(gender, 'lady,' ) WHERE idUser = '$idusr'";
			$req4 = $bdd->prepare($sql4);
			$req4->execute();
			$req4->closeCursor();
		}
		if($android != 3)
		{
			$sql4= "UPDATE moreusers SET gender=CONCAT(gender, 'android,' ) WHERE idUser = '$idusr'";
			$req4 = $bdd->prepare($sql4);
			$req4->execute();
			$req4->closeCursor();
		}
		if($other != 3)
		{
			$sql4= "UPDATE moreusers SET gender=CONCAT(gender, 'other,' ) WHERE idUser = '$idusr'";
			$req4 = $bdd->prepare($sql4);
			$req4->execute();
			$req4->closeCursor();
		}

		/* INTEREST */
		$sql3= "UPDATE moreusers SET interest=',' WHERE idUser = '$idusr'";
		$req3 = $bdd->prepare($sql3);
		$req3->execute();
		$req3->fetch();
		$req3->closeCursor();
		if ($man2 != 3)
		{
			$sql2= "UPDATE moreusers SET interest=CONCAT(interest, 'man,' ) WHERE idUser = '$idusr'";
			$req2 = $bdd->prepare($sql2);
			$req2->execute();
			$req2->closeCursor();
		}
		if($lady2 != 3)
		{
			$sql4= "UPDATE moreusers SET interest=CONCAT(interest, 'lady,' ) WHERE idUser = '$idusr'";
			$req4 = $bdd->prepare($sql4);
			$req4->execute();
			$req4->closeCursor();
		}
		if($android2 != 3)
		{
			$sql4= "UPDATE moreusers SET interest=CONCAT(interest, 'android,' ) WHERE idUser = '$idusr'";
			$req4 = $bdd->prepare($sql4);
			$req4->execute();
			$req4->closeCursor();
		}
		if($other2 != 3)
		{
			$sql4= "UPDATE moreusers SET interest=CONCAT(interest, 'other,' ) WHERE idUser = '$idusr'";
			$req4 = $bdd->prepare($sql4);
			$req4->execute();
			$req4->closeCursor();
		}
		$sql3= "UPDATE moreusers SET Updated='1' WHERE idUser = '$idusr'";
                $req3 = $bdd->prepare($sql3);
                $req3->execute();
                $req3->fetch();
                $req3->closeCursor();
		header("Location: ../pages/moreprofile.php?success=changeok");
		exit();
	}
}

/* BIOGRAPHY */

if (isset($_POST['bio-submit']))
{
	$idusr = $_SESSION['idUser'];
	$bdd = connect();
	if (!empty($_POST['bio']))
	{
		$bio = htmlspecialchars(addslashes($_POST['bio']));
		$sql = "UPDATE moreusers SET
			Bio = '$bio' WHERE idUser = '$idusr'" ;
		$req = $bdd->prepare($sql);
		$req->execute();
		header("Location: ../pages/moreprofile.php?success=biook");
		$req->closeCursor();
		exit();

	}
	else
	{
		$uidusr= $_SESSION['uidUser'];
		$sql = "UPDATE moreusers SET
			Bio = 'Hello my name is $uidusr, I am the android sent by Cyberlife' WHERE idUser = '$idusr'" ;
		$req = $bdd->prepare($sql);
		$req->execute();
		header("Location: ../pages/moreprofile.php?success=emptyok");
		$req->closeCursor();
		exit();
	}
}

/* CHANGE INTERESTS */
if (isset($_POST['interests-submit']))
{
	if (isset($_POST['sports']))
		$sports = $_POST['sports'];
	else
		$sports=3;
	if (isset($_POST['blogging']))
		$blogging = $_POST['blogging'];
	else
		$blogging=3;
	if (isset($_POST['volunteering']))
		$volunteering = $_POST['volunteering'];
	else
		$volunteering=3;
	if (isset($_POST['art']))
		$art = $_POST['art'];
	else
		$art=3;

	if (isset($_POST['music']))
		$music = $_POST['music'];
	else
		$music=3;
	if (isset($_POST['reading']))
		$reading = $_POST['reading'];
	else
		$reading=3;
	if (isset($_POST['vgaming']))
		$vgaming = $_POST['vgaming'];
	else
		$vgaming=3;
	if (isset($_POST['sgaming']))
		$sgaming = $_POST['sgaming'];
	else
		$sgaming=3;

	if (isset($_POST['cleaning']))
		$cleaning = $_POST['cleaning'];
	else
		$cleaning=3;
	if (isset($_POST['licking']))
		$licking = $_POST['licking'];
	else
		$licking=3;
	if (isset($_POST['RA9']))
		$RA9 = $_POST['RA9'];
	else
		$RA9=3;
	if (isset($_POST['eden']))
		$eden = $_POST['eden'];
	else
		$eden=3;
	$idusr = $_SESSION['idUser'];
	$bdd = connect();
	        $sql = "SELECT idUser FROM moreusers WHERE idUser = '$idusr'";
        $req = $bdd->prepare($sql);
        $req->execute();
        $req->bindColumn(1, $exist);
        $req->fetch();
        if (!isset($exist))
        {
                $sql = "INSERT INTO moreusers (idUser, gender, interest) VALUES ('$idusr', 'android', 'man,lady,android')" ;
                $req = $bdd->prepare($sql);
                $req->execute();
        }
	if ($sports==3 && $blogging==3 && $volunteering==3 && $art==3 && $music==3 && $reading==3 && $vgaming==3 && $sgaming==3 && $cleaning==3 && $licking==3 && $RA9==3 && $eden==3)
	{
		$sql = "UPDATE moreusers SET
			tags = '#RA9' WHERE idUser = '$idusr'" ;
		$req = $bdd->prepare($sql);
		$req->execute();
		header("Location: ../pages/profile.php?error=emptyfields");
		$req->closeCursor();
		exit();
	}
	else
	{
		$sql3= "UPDATE moreusers SET tags=',' WHERE idUser = '$idusr'";
		$req3 = $bdd->prepare($sql3);
		$req3->execute();
		$req3->fetch();
		$req3->closeCursor();
		if ($sports != 3)
		{
			$sql2= "UPDATE moreusers SET tags=CONCAT(tags, '#Sports,' ) WHERE idUser = '$idusr'";
			$req2 = $bdd->prepare($sql2);
			$req2->execute();
			$req2->closeCursor();
		}
		if($blogging != 3)
		{
			$sql4= "UPDATE moreusers SET tags=CONCAT(tags, '#Blogging,' ) WHERE idUser = '$idusr'";
			$req4 = $bdd->prepare($sql4);
			$req4->execute();
			$req4->closeCursor();
		}
		if($volunteering != 3)
		{
			$sql4= "UPDATE moreusers SET tags=CONCAT(tags, '#Volunteering,' ) WHERE idUser = '$idusr'";
			$req4 = $bdd->prepare($sql4);
			$req4->execute();
			$req4->closeCursor();
		}
		if($music != 3)
		{
			$sql4= "UPDATE moreusers SET tags=CONCAT(tags, '#Music,' ) WHERE idUser = '$idusr'";
			$req4 = $bdd->prepare($sql4);
			$req4->execute();
			$req4->closeCursor();
		}
		if ($vgaming != 3)
		{
			$sql2= "UPDATE moreusers SET tags=CONCAT(tags, '#VideoGaming,' ) WHERE idUser = '$idusr'";
			$req2 = $bdd->prepare($sql2);
			$req2->execute();
			$req2->closeCursor();
		}
		if($sgaming != 3)
		{
			$sql4= "UPDATE moreusers SET tags=CONCAT(tags, '#StrategicGaming,' ) WHERE idUser = '$idusr'";
			$req4 = $bdd->prepare($sql4);
			$req4->execute();
			$req4->closeCursor();
		}
		if($cleaning != 3)
		{
			$sql4= "UPDATE moreusers SET tags=CONCAT(tags, '#Cleaning,' ) WHERE idUser = '$idusr'";
			$req4 = $bdd->prepare($sql4);
			$req4->execute();
			$req4->closeCursor();
		}
		if($licking != 3)
		{
			$sql4= "UPDATE moreusers SET tags=CONCAT(tags, '#LickingThings,' ) WHERE idUser = '$idusr'";
			$req4 = $bdd->prepare($sql4);
			$req4->execute();
			$req4->closeCursor();
		}
		if($RA9 != 3)
		{
			$sql4= "UPDATE moreusers SET tags=CONCAT(tags, '#RA9,' ) WHERE idUser = '$idusr'";
			$req4 = $bdd->prepare($sql4);
			$req4->execute();
			$req4->closeCursor();
		}
		if($eden != 3)
		{
			$sql4= "UPDATE moreusers SET tags=CONCAT(tags, '#EdenClub,' ) WHERE idUser = '$idusr'";
			$req4 = $bdd->prepare($sql4);
			$req4->execute();
			$req4->closeCursor();
		}
		if($art != 3)
		{
			$sql4= "UPDATE moreusers SET tags=CONCAT(tags, '#ArtAndDesign,' ) WHERE idUser = '$idusr'";
			$req4 = $bdd->prepare($sql4);
			$req4->execute();
			$req4->closeCursor();
		}
		if($reading != 3)
		{
			$sql4= "UPDATE moreusers SET tags=CONCAT(tags, '#Reading,' ) WHERE idUser = '$idusr'";
			$req4 = $bdd->prepare($sql4);
			$req4->execute();
			$req4->closeCursor();
		}
		header("Location: ../pages/profile.php?test=interestok");
		exit();
	}
}
