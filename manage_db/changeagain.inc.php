<?php
require "../config/connect.php";
require "mailActivation.php";
require "../pages/header.php";

$username = $_SESSION["uidUsers"];
$keyff = $_SESSION["keyf"];

//MODIFY PASSWORD
if (isset($_POST['modifypwdagain-submit']))
{
    $newpass = $_POST['newpass'];
    $newpass2 = $_POST['newpass2'];

    $bdd = connect();
    $sql = "SELECT * FROM users WHERE login= :username";
    $req = $bdd->prepare($sql);
    $req->execute(['username' => $username]);
    if (empty($newpass) || empty($newpass2))
    {
        header("Location: ../pages/changePwdLink.php?error=emptyfields");
        exit();
	}
	if($row =$req->fetch())
{
    $req->closeCursor();
    $keybdd = $row['keyf'];
	}
      if($keyff !== $keybdd) // On compare nos deux clés
       {
		   header("Location: ../pages/changePwdLink.php?error=keyinvalid");
	  }
    else if (!preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,}$#', $newpass))
    {
            header("Location: ../pages/changePwdLink.php?error=invalidpassword");
            exit();
     	}
        else if ($newpass !== $newpass2)
        {
            header("Location: ../pages/changePwdLink.php?error=passwordcheck");
            exit();
        }
        else
        {
            $hashedNewPwd = password_hash($newpass, PASSWORD_DEFAULT);
            $sql2 = "UPDATE users SET password=:hashedNewPwd WHERE login='$username'";
            $req2 = $bdd->prepare($sql2);
            $req2->execute(['hashedNewPwd' => $hashedNewPwd]);
			$req2->closeCursor();
			session_unset();
        	session_destroy();
            header("Location: ../pages/changePwdLink.php?change=passwordsuccess");
            exit();
        }
    }

?>

