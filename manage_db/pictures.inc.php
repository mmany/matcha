<?php
require "../pages/header.php";
require "../config/connect.php";

if (isset($_POST['picture1-submit']))
{
	$username=$_SESSION['uidUser'];
	$idusr = $_SESSION['idUser'];
	$imagename = 'picture1.png';
	$imagetype = $_FILES['fic']['type'];
	$imageerror = $_FILES['fic']['error'];
	$imagetemp = $_FILES['fic']['tmp_name'];

	$imagePath = "../imgs/profile/$username/";

	if(is_uploaded_file($imagetemp)) {
		if(move_uploaded_file($imagetemp, $imagePath . $imagename)) {
			$bdd=connect();
			$sql = "UPDATE images SET
				image1 = '../imgs/profile/$username/picture1.png' WHERE idUser = '$idusr'" ;
			$req = $bdd->prepare($sql);
			$req->execute();
			header("Location: ../pages/picture1.php?success=importok");
			exit();
		}
		else {
			header("Location: ../pages/picture1.php?error=moveoff");
			exit();
		}
	}
	else {
		header("Location: ../pages/picture1.php?error=uploadoff");
		exit();
	}
}
if (isset($_POST['picture2-submit']))
{
        $username=$_SESSION['uidUser'];
        $idusr = $_SESSION['idUser'];
        $imagename = 'picture2.png';
        $imagetype = $_FILES['fic']['type'];
        $imageerror = $_FILES['fic']['error'];
        $imagetemp = $_FILES['fic']['tmp_name'];

        $imagePath = "../imgs/profile/$username/";

        if(is_uploaded_file($imagetemp)) {
                if(move_uploaded_file($imagetemp, $imagePath . $imagename)) {
                        $bdd=connect();
                        $sql = "UPDATE images SET
                                image2 = '../imgs/profile/$username/picture2.png' WHERE idUser = '$idusr'" ;
                        $req = $bdd->prepare($sql);
                        $req->execute();
                        header("Location: ../pages/picture2.php?success=importok");
                        exit();
                }
                else {
                        header("Location: ../pages/picture2.php?error=moveoff");
                        exit();
                }
        }
        else {
                header("Location: ../pages/picture2.php?error=uploadoff");
                exit();
        }
}
if (isset($_POST['picture3-submit']))
{
        $username=$_SESSION['uidUser'];
        $idusr = $_SESSION['idUser'];
        $imagename = 'picture3.png';
        $imagetype = $_FILES['fic']['type'];
        $imageerror = $_FILES['fic']['error'];
        $imagetemp = $_FILES['fic']['tmp_name'];

        $imagePath = "../imgs/profile/$username/";

        if(is_uploaded_file($imagetemp)) {
                if(move_uploaded_file($imagetemp, $imagePath . $imagename)) {
                        $bdd=connect();
                        $sql = "UPDATE images SET
                                image3 = '../imgs/profile/$username/picture3.png' WHERE idUser = '$idusr'" ;
                        $req = $bdd->prepare($sql);
                        $req->execute();
                        header("Location: ../pages/picture3.php?success=importok");
                        exit();
                }
                else {
                        header("Location: ../pages/picture3.php?error=moveoff");
                        exit();
                }
        }
        else {
                header("Location: ../pages/picture3.php?error=uploadoff");
                exit();
        }
}
if (isset($_POST['picture4-submit']))
{
        $username=$_SESSION['uidUser'];
        $idusr = $_SESSION['idUser'];
        $imagename = 'picture4.png';
        $imagetype = $_FILES['fic']['type'];
        $imageerror = $_FILES['fic']['error'];
        $imagetemp = $_FILES['fic']['tmp_name'];

        $imagePath = "../imgs/profile/$username/";

        if(is_uploaded_file($imagetemp)) {
                if(move_uploaded_file($imagetemp, $imagePath . $imagename)) {
                        $bdd=connect();
                        $sql = "UPDATE images SET
                                image4 = '../imgs/profile/$username/picture4.png' WHERE idUser = '$idusr'" ;
                        $req = $bdd->prepare($sql);
                        $req->execute();
                        header("Location: ../pages/picture4.php?success=importok");
                        exit();
                }
                else {
                        header("Location: ../pages/picture4.php?error=moveoff");
                        exit();
                }
        }
        else {
                header("Location: ../pages/picture4.php?error=uploadoff");
                exit();
        }
}
if (isset($_POST['picture5-submit']))
{
        $username=$_SESSION['uidUser'];
        $idusr = $_SESSION['idUser'];
        $imagename = 'picture5.png';
        $imagetype = $_FILES['fic']['type'];
        $imageerror = $_FILES['fic']['error'];
        $imagetemp = $_FILES['fic']['tmp_name'];

        $imagePath = "../imgs/profile/$username/";

        if(is_uploaded_file($imagetemp)) {
                if(move_uploaded_file($imagetemp, $imagePath . $imagename)) {
                        $bdd=connect();
                        $sql = "UPDATE images SET
                                image5 = '../imgs/profile/$username/picture5.png' WHERE idUser = '$idusr'" ;
                        $req = $bdd->prepare($sql);
                        $req->execute();
                        header("Location: ../pages/picture5.php?success=importok");
                        exit();
                }
                else {
                        header("Location: ../pages/picture5.php?error=moveoff");
                        exit();
                }
        }
        else {
                header("Location: ../pages/picture5.php?error=uploadoff");
                exit();
        }
}
