<?php
require "../pages/header.php";
require "../config/connect.php";

if (isset($_POST['login-submit']))
{
	$uid = $_POST['uid'];
	$password = $_POST['pwd'];

	if (empty($uid) || empty($password))
	{
		header("Location: ../pages/login.php?error=emptyfields");
		exit();
	}
	else
	{
		$bdd = connect();
		$sql = "SELECT * FROM users WHERE login= :uid";
		$req = $bdd->prepare($sql);
		$req->execute(['uid' => $uid]);
		if ($row = $req->fetch())
		{
			$pwdCheck = password_verify($password, $row['password']);
			$activated = $row['activated'];
			if ($pwdCheck == false)
			{
				header("Location: ../pages/login.php?error=wrongpwd");
				exit();
			}
			else if ($activated == 0)
			{
				header("Location: ../pages/login.php?error=noactivated");
				exit();
			}
			else if ($pwdCheck == true)
			{
				$_SESSION['uidUser'] = $row['login'];
				$_SESSION['emailUser'] = $row['email'];
				$_SESSION['pwdUser'] = $row['password'];
				$_SESSION['idUser'] = $row['id'];
				$_SESSION['FnameUser'] = $row['firstname'];
				$_SESSION['LnameUser'] = $row['name'];
				$iduser = $_SESSION['idUser'];
				$sql3= "UPDATE moreusers SET isonline='1' WHERE idUser = '$iduser'";
				$req3 = $bdd->prepare($sql3);
				$req3->execute();
				$req3->fetch();
				$req3->closeCursor();
				header("Location: ../pages/profile.php?login=success");
				exit();
			}
		}
		else
		{
			header("Location: ../pages/login.php?error=nouser");
			exit();
		}
	}
}
?>

