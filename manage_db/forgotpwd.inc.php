<?php
require "../pages/header.php";
require "mailActivation.php";
require "../config/connect.php";

if (isset($_POST['forgotpwd-submit']))
{
    $mail = $_POST['mail'];

    if (empty($mail))
    {
        header("Location: ../pages/forgotPwd.php?error=emptyfields");
        exit();
	}
	else if (!filter_var($mail, FILTER_VALIDATE_EMAIL))
    {
        header("Location: ../pages/forgotPwd.php?error=invalidmail");
        exit();
    }
    $bdd = connect();
    $sql = "SELECT email FROM users WHERE email=:mail";
    $req = $bdd->prepare($sql);
    $req->execute(['mail' => $mail]);
    if($mail = $req->fetch()['email']) //check if a line contain the same email
	{
		$req->closeCursor();
		$req = $bdd->prepare("SELECT keyf FROM users WHERE email= :mail");
		$req->execute(['mail' => $mail]);
		$keyfs = $req->fetch()['keyf'];
		$req->closeCursor();
        $req = $bdd->prepare("SELECT login FROM users WHERE email= :mail");
        $req->execute(['mail' => $mail]);
        $username = $req->fetch()['login'];
		forgotMailPwd($mail, $keyfs, $username);
		header("Location: ../pages/forgotPwd.php?success=emailfound");
        $req->closeCursor();
        exit();
    }
    else
    {
        header("Location: ../pages/forgotPwd.php?error=nomailfound");
        exit();
    }
}
?>
