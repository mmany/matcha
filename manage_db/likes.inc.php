<?php
require "../pages/header.php";
require "../config/connect.php";

$bdd = connect();

if (isset($_POST['like-submit']))
{
	$idliked = $_POST['isliked'];
	$idlikeur = $_POST['idlikeur'];
	$uidliked = $_POST['loginliked'];

	if (empty($idlikeur))
	{
		header("Location: ../profile.php?error=needlogin");
		exit();
	}
	/*CHECK IF ALREADY LIKED*/
	$sql = "SELECT IdLike, IsLike FROM managelike WHERE idUid = '$idliked' AND idLikeur = '$idlikeur'";
	$req = $bdd->prepare($sql);
	$req->execute();
	$req->bindColumn(1, $exist);
	$req->bindColumn(2, $value);
	$req->fetch();
	$sql = "SELECT IdLike, IsLike FROM managelike WHERE idUid = '$idlikeur' AND idLikeur = '$idliked'";
	$req = $bdd->prepare($sql);
	$req->execute();
	$req->bindColumn(1, $exist2);
	$req->bindColumn(2, $value2);
	$req->fetch();
	if ($exist && $value == 1)
	{
		//UNLIKE
		$sql3= "UPDATE managelike SET IsLike='0' WHERE idUid = '$idliked' AND idLikeur = '$idlikeur'";
		$req3 = $bdd->prepare($sql3);
		$req3->execute();
		$req3->fetch();
		$req3->closeCursor();
		$sql7= "SELECT COUNT(*) FROM Blocked WHERE blockeur='$idliked' AND blocked='$idlikeur'";
		$req7 = $bdd->prepare($sql7);
		$req7->execute();
		$req7->bindColumn(1, $blocked);
		$req7->fetch();
		$req7->closeCursor();
		if ($blocked == 0)
		{
			$sql3 = "INSERT INTO notifications (emitter, receiver, text, seen) VALUES ('$idlikeur', '$idliked', 'unliked you', '0')" ;
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}

		//UNMATCH
		if ($exist2)
		{
			$sql3= "UPDATE managelike SET IsMatcha='0' WHERE idUid = '$idliked' AND idLikeur = '$idlikeur'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
			$sql3= "UPDATE managelike SET IsMatcha='0' WHERE idUid = '$idlikeur' AND idLikeur = '$idliked'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		header("Location: ../pages/oprofile.php?login=$uidliked");
		exit();
	}
	else if ($exist && $value == 0)
	{
		//LIKE
		$sql3= "UPDATE managelike SET IsLike='1' WHERE idUid = '$idliked' AND idLikeur = '$idlikeur'";
		$req3 = $bdd->prepare($sql3);
		$req3->execute();
		$req3->fetch();
		$req3->closeCursor();
		$sql7= "SELECT COUNT(*) FROM Blocked WHERE blockeur='$idliked' AND blocked='$idlikeur'";
		$req7 = $bdd->prepare($sql7);
		$req7->execute();
		$req7->bindColumn(1, $blocked);
		$req7->fetch();
		$req7->closeCursor();
		if ($blocked == 0)
		{
			$sql3 = "INSERT INTO notifications (emitter, receiver, text, seen) VALUES ('$idlikeur', '$idliked', 'liked you', '0')" ;
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}

		//MATCH
		if ($exist2 && $value2 == 1)
		{
			$sql3= "UPDATE managelike SET IsMatcha='1' WHERE idUid = '$idliked' AND idLikeur = '$idlikeur'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
			$sql3= "UPDATE managelike SET IsMatcha='1' WHERE idUid = '$idlikeur' AND idLikeur = '$idliked'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
			$sql7= "SELECT COUNT(*) FROM Blocked WHERE blockeur='$idliked' AND blocked='$idlikeur'";
			$req7 = $bdd->prepare($sql7);
			$req7->execute();
			$req7->bindColumn(1, $blocked);
			$req7->fetch();
			$req7->closeCursor();
			if ($blocked == 0)
			{
				$sql3 = "INSERT INTO notifications (emitter, receiver, text, seen) VALUES ('$idlikeur', '$idliked', 'matched you', '0')" ;
				$req3 = $bdd->prepare($sql3);
				$req3->execute();
				$req3->fetch();
				$req3->closeCursor();
			}
		}
		header("Location: ../pages/oprofile.php?login=$uidliked");
		exit();
	}
	else{
		$sql = "INSERT INTO managelike (idUid, idLikeur, IsLike) VALUES ($idliked, $idlikeur, 1)";
		$req = $bdd->prepare($sql);
		$req->execute();
		$req->closeCursor();
		$sql7= "SELECT COUNT(*) FROM Blocked WHERE blockeur='$idliked' AND blocked='$idlikeur'";
		$req7 = $bdd->prepare($sql7);
		$req7->execute();
		$req7->bindColumn(1, $blocked);
		$req7->fetch();
		$req7->closeCursor();
		if ($blocked == 0)
		{
			$sql3 = "INSERT INTO notifications (emitter, receiver, text, seen) VALUES ('$idlikeur', '$idliked', 'liked you', '0')" ;
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}

		//MATCH
		if ($exist2 && $value2 == 1)
		{
			$sql3= "UPDATE managelike SET IsMatcha='1' WHERE idUid = '$idliked' AND idLikeur = '$idlikeur'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
			$sql3= "UPDATE managelike SET IsMatcha='1' WHERE idUid = '$idlikeur' AND idLikeur = '$idliked'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
			$sql7= "SELECT COUNT(*) FROM Blocked WHERE blockeur='$idliked' AND blocked='$idlikeur'";
			$req7 = $bdd->prepare($sql7);
			$req7->execute();
			$req7->bindColumn(1, $blocked);
			$req7->fetch();
			$req7->closeCursor();
			if ($blocked == 0)
			{
				$sql3 = "INSERT INTO notifications (emitter, receiver, text, seen) VALUES ('$idlikeur', '$idliked', 'matched you', '0')" ;
				$req3 = $bdd->prepare($sql3);
				$req3->execute();
				$req3->fetch();
				$req3->closeCursor();
			}
		}
		header("Location: ../pages/oprofile.php?login=$uidliked");
		exit();
	}
}
?>
