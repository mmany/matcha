<?php
require "../config/connect.php";

if (isset($_GET['logout']) && $_GET['logout'] == 'true')
{
	session_start();
	$bdd = connect();
	$iduser = $_SESSION['idUser'];
	$sql3= "UPDATE moreusers SET lastonline=CURRENT_TIMESTAMP, isonline='0' WHERE idUser = '$iduser'";
	$req3 = $bdd->prepare($sql3);
	$req3->execute();
	$req3->fetch();
	$req3->closeCursor();
	session_unset();
	session_destroy();
	header("Location: ../pages/login.php?profile=logout");
	exit();
}
?>
