<?php
require "../pages/header.php";
require "mailActivation.php";
require "../config/connect.php";
$login2 = $_SESSION['uidUser'];
$mineid = $_SESSION['idUser'];


//REPORT
if (isset($_POST['report-submit']))
{
	$report = htmlspecialchars($_POST['report']);
	$reported = $_POST['reported'];

        if (empty($report) || empty($reported))
        {
                header("Location: ../pages/report.php?login=$reported&error=emptyfields");
                exit();
	}
	else
	{
		reportProfile($login2, $report, $reported);
		header("Location: ../pages/report.php?login=$reported&success=reported");
		exit();
	}
}

//BLOCK
if (isset($_POST['block-submit']))
{
        $block = $_POST['blocked'];

        if (empty($block))
        {
                header("Location: ../pages/block.php?login=$block&error=emptyfields");
                exit();
        }
        else
	{
		$bdd = connect();
		$sql7= "SELECT id FROM users WHERE login='$block'";
                $req7 = $bdd->prepare($sql7);
                $req7->execute();
                $req7->bindColumn(1, $idblocked);
                $req7->fetch();
                $req7->closeCursor();
                $sql7= "SELECT COUNT(*) FROM Blocked WHERE blockeur='$mineid' AND blocked='$idblocked'";
                $req7 = $bdd->prepare($sql7);
                $req7->execute();
                $req7->bindColumn(1, $isblocked);
                $req7->fetch();
                $req7->closeCursor();
                if ($isblocked != 0)
                {
			header("Location: ../pages/block.php?login=$block&error=alreadyblocked");
			exit();
		}
		else
		{
		$sql7= "INSERT INTO `blocked`(`blockeur`, `blocked`) VALUES ('$mineid','$idblocked')";
                $req7 = $bdd->prepare($sql7);
                $req7->execute();
                $req7->fetch();
                $req7->closeCursor();
                header("Location: ../pages/block.php?login=$block&success=blocked");
                exit();
		}
	}
}
