<?php
require "../pages/header.php";
require "mailActivation.php";
require "../config/connect.php";
$idusr = $_SESSION['idUser'];


//MODIFY NAME
if (isset($_POST['setname-submit']))
{
	$fname = addslashes($_POST['fname']);
	$lname =  addslashes($_POST['lname']);

	if (empty($fname) || empty($lname))
	{
		header("Location: ../pages/settings.php?error=emptyfields");
		exit();
	}
	else if (!preg_match("/^[a-zA-Z0-9]*$/", $fname) && !preg_match("/^[a-zA-Z0-9]*$/", $lname))
	{
		header("Location: ../pages/settings.php?error=invalidname");
		exit();
	}
	else
	{
		$bdd = connect();
		$sql3= "UPDATE users SET firstname='$fname', name='$lname' WHERE id = '$idusr'";
		$req3 = $bdd->prepare($sql3);
		$req3->execute();
		$req3->fetch();
		$req3->closeCursor();
		header("Location: ../pages/settings.php?success=nameok");
		exit();
	}
}

//MODIFY MAIL
if (isset($_POST['setmail-submit']))
{
	$newmail = $_POST['mail'];

	if (empty($newmail))
	{
		header("Location: ../pages/settings.php?error=emptyfields");
		exit();
	}
	else if (!filter_var($newmail, FILTER_VALIDATE_EMAIL))
	{
		header("Location: ../pages/settings.php?error=invalidmail");
		exit();
	}
	$bdd = connect();
	$sql = "SELECT email FROM users WHERE email= :newmail";
	$req = $bdd->prepare($sql);
	$req->execute(['newmail' => $newmail]);
	if($checkname = $req->fetch()) //check if a line contain the same email
	{
		header("Location: ../pages/settings.php?error=emailtaken");
		$req->closeCursor();
		exit();
	}
	else
	{
		$username= $_SESSION['uidUser'];
		$newkeyf = md5(microtime(TRUE)*100000);
		//	changingMail($username, $newmail, $newkeyf, $oldmail);
		//	changeOld($username, $oldmail);
		$sql = "UPDATE `users` SET `email`= :newmail, `keyf`= :newkeyf  WHERE login='$username'";
		$req = $bdd->prepare($sql);
		$req->execute(['newmail' => $newmail, 'newkeyf' => $newkeyf]);
		$req->closeCursor();
		$_SESSION['emailUsers']=$newmail;
		header("Location: ../pages/settings.php?success=mailok");
		exit();
	}
}

//MODIFY PASSWORD
if (isset($_POST['setpass-submit']))
{
	$newpass = $_POST['pwd'];
	$newpass2 = $_POST['pwd-repeat'];

	$username= $_SESSION['uidUser'];
	$bdd = connect();
	$sql = "SELECT * FROM users WHERE login= :username";
	$req = $bdd->prepare($sql);
	$req->execute(['username' => $username]);
	if (empty($newpass) || empty($newpass2))
	{
		header("Location: ../pages/settings.php?error=emptyfields");
		exit();
	}
	if ($row = $req->fetch())
	{
		$req->closeCursor();
		if (!preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,}$#', $newpass))
		{
			header("Location: ../pages/settings.php?error=invalidpassword");
			exit();
		}
		else if ($newpass !== $newpass2)
		{
			header("Location: ../pages/settings.php?error=passwordcheck");
			exit();
		}
		else
		{
			$hashedNewPwd = password_hash($newpass, PASSWORD_DEFAULT);
			$sql2 = "UPDATE users SET password=:hashedNewPwd WHERE login='$username'";
			$req2 = $bdd->prepare($sql2);
			$req2->execute(['hashedNewPwd' => $hashedNewPwd]);
			$req2->closeCursor();
			$_SESSION['pwdUser']= $hashedNewPwd;
			header("Location: ../pages/settings.php?success=passwordoks");
			exit();
		}
	}
}

//MODIFY LOCALISATION
if (isset($_POST['setlocal-submit']))
{
	$local =  addslashes($_POST['local']);

	if (empty($local))
	{
		header("Location: ../pages/settings.php?error=emptyfields");
		exit();
	}
	if($local != "Paris" && $local != "Marseille" && $local != "Lyon" && $local != "Toulouse" && $local != "Nice" && $local != "Nantes" && $local != "Strasbourg" && $local != "Reims" && $local != "Philadelphia" && $local != "Florida" && $local != "Los Angeles" && $local != "Miami" && $local != "Madrid" && $local != "Barcelona" && $local != "Berlin" && $local != "Stockholm" && $local != "Helsinki" && $local != "Nairobi" && $local != "Denver" && $local != "Tokyo" && $local != "Rio" && $local != "Moscou" && $local != "Oslo" && $local != "Palerme" && $local != "London" && $local != "Reykjavik" && $local != "Chicago" && $local != "San Francisco" && $local != "Seoul" && $local != "Mexico City" && $local != "Mumbai" && $local != "Detroit")
	{
		header("Location: ../pages/settings.php?error=invalid");
		exit();
	}
	else
	{
		$bdd = connect();
		$sql3= "UPDATE moreusers SET localisation='$local' WHERE idUser = '$idusr'";
		$req3 = $bdd->prepare($sql3);
		$req3->execute();
		$req3->fetch();
		$req3->closeCursor();
		if($local == "Paris")
		{
			$sql3= "UPDATE moreusers SET country='France', latitude='48.8566', longitude='2.35222' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Marseille")
		{
			$sql3= "UPDATE moreusers SET country='France', latitude='43.2965', longitude='5.3698' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Lyon")
		{
			$sql3= "UPDATE moreusers SET country='France', latitude='45.7640', longitude='4.8357' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Toulouse")
		{
			$sql3= "UPDATE moreusers SET country='France', latitude='43.6047', longitude='1.4442' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Nice")
		{
			$sql3= "UPDATE moreusers SET country='France', latitude='43.7102', longitude='7.2619' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Nantes")
		{
			$sql3= "UPDATE moreusers SET country='France', latitude='47.2184', longitude='-1.5536' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Strasbourg")
		{
			$sql3= "UPDATE moreusers SET country='France', latitude='48.5734', longitude='7.7521' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Montpellier")
		{
			$sql3= "UPDATE moreusers SET country='France', latitude='43.6108', longitude='3.8767' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Bordeaux")
		{
			$sql3= "UPDATE moreusers SET country='France', latitude='44.8378', longitude='-0.5791' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Lilles")
		{
			$sql3= "UPDATE moreusers SET country='France', latitude='50.6293', longitude='3.0573' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Rennes")
		{
			$sql3= "UPDATE moreusers SET country='France', latitude='48.1173', longitude='-1.6777' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Reims")
		{
			$sql3= "UPDATE moreusers SET country='France', latitude='49.2583', longitude='4.0317' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Philadelphia")
		{
			$sql3= "UPDATE moreusers SET country='United-States', latitude='39.9526', longitude='-75.1652' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Florida")
		{
			$sql3= "UPDATE moreusers SET country='United-States', latitude='32.3608', longitude='-77.8049' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Los Angeles")
		{
			$sql3= "UPDATE moreusers SET country='United-States', latitude='34.0522', longitude='-118.2437' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Miami")
		{
			$sql3= "UPDATE moreusers SET country='United-States', latitude='25.7617', longitude='-80.1917' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Madrid")
		{
			$sql3= "UPDATE moreusers SET country='Spain', latitude='40.4168', longitude='-3.7037' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Barcelona")
		{
			$sql3= "UPDATE moreusers SET country='Spain', latitude='41.3850', longitude='2.1734' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Berlin")
		{
			$sql3= "UPDATE moreusers SET country='Germany', latitude='52.5200', longitude='13.4049' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Stockholm")
		{
			$sql3= "UPDATE moreusers SET country='Sweden', latitude='59.3293', longitude='18.0686' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Helsinki")
		{
			$sql3= "UPDATE moreusers SET country='Finland', latitude='60.1699', longitude='24.9384' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Nairobi")
		{
			$sql3= "UPDATE moreusers SET country='Kenya', latitude='-1.2920', longitude='36.8219' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Denver")
		{
			$sql3= "UPDATE moreusers SET country='United-States', latitude='39.7392', longitude='-104.9902' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Tokyo")
		{
			$sql3= "UPDATE moreusers SET country='Japan', latitude='36.4408', longitude='141.2405' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Rio")
		{
			$sql3= "UPDATE moreusers SET country='Brasil', latitude='-22.9068', longitude='-43.1729' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Moscou")
		{
			$sql3= "UPDATE moreusers SET country='Russia', latitude='56.0215', longitude='37.9678' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Oslo")
		{
			$sql3= "UPDATE moreusers SET country='Norway', latitude='59.8097', longitude='10.6226' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Palerme")
		{
			$sql3= "UPDATE moreusers SET country='Italy', latitude='38.1157', longitude='13.3615' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "London")
		{
			$sql3= "UPDATE moreusers SET country='United-Kingdom', latitude='51.5074', longitude='-0.1277' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Reykjavik")
		{
			$sql3= "UPDATE moreusers SET country='Iceland', latitude='64.1466', longitude='-21.9426' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Chicago")
		{
			$sql3= "UPDATE moreusers SET country='United-States', latitude='41.8781', longitude='-87.6297' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "San Francisco")
		{
			$sql3= "UPDATE moreusers SET country='United-States', latitude='37.7749', longitude='-122.4194' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Seoul")
		{
			$sql3= "UPDATE moreusers SET country='South Korea', latitude='37.5665', longitude='126.9779' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Mexico City")
		{
			$sql3= "UPDATE moreusers SET country='Mexico', latitude='19.4326', longitude='-99.1332' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Mumbai")
		{
			$sql3= "UPDATE moreusers SET country='India', latitude='19.0759', longitude='72.8777' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		else if($local == "Detroit")
		{
			$sql3= "UPDATE moreusers SET country='United-States', latitude='42.3314', longitude='-83.0457' WHERE idUser = '$idusr'";
			$req3 = $bdd->prepare($sql3);
			$req3->execute();
			$req3->fetch();
			$req3->closeCursor();
		}
		header("Location: ../pages/settings.php?success=localok");
		exit();
	}
}

//MODIFY AGE
if (isset($_POST['setage-submit']))
{
	$age =  $_POST['age'];

	if (empty($age))
	{
		header("Location: ../pages/settings.php?error=emptyfields");
		exit();
	}
	else if (!preg_match("/^([0-9]*)$/", $age))
	{
		header("Location: ../pages/settings.php?error=invalidage");
		exit();
	}
	else if ($age > 2147483647)
	{
		header("Location: ../pages/settings.php?error=invalidage");
		exit();
	}
	else
	{
		$bdd = connect();
		$sql3= "UPDATE moreusers SET Age='$age' WHERE idUser = '$idusr'";
		$req3 = $bdd->prepare($sql3);
		$req3->execute();
		$req3->fetch();
		$req3->closeCursor();
		header("Location: ../pages/settings.php?success=ageok");
		exit();
	}
}
