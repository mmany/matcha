<?php
function connect(){
        require "database.php";
        try{
                $bdd = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
                $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $bdd->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        }
        catch(PDOException $e){
                echo "La base de donnée n'est pas disponible, merci de rééssayer plus tard.\n";
        }
        return($bdd);
}
?>
