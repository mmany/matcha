<?php
require "database.php";
require "connect.php";

$bdd2 = new PDO('mysql:'.$DB_HOST, $DB_USER, $DB_PASSWORD);
$bdd2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$bdd2->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$sql = 'DROP DATABASE IF EXISTS db_matcha';
$req = $bdd2->prepare($sql);
$result = $req->execute();
$sql = 'CREATE DATABASE IF NOT EXISTS db_matcha';
$req = $bdd2->prepare($sql);
$result = $req->execute();
if ($result)
{
	echo "Base de données créée correctement.\n";
} 
else
{
	echo "Erreur lors de la création de la base de données \n";
}
$req->closeCursor();

$bdd = connect();
$sql1 = "CREATE TABLE IF NOT EXISTS Users (
	id int (11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
	firstname TINYTEXT NOT NULL,
	name TINYTEXT NOT NULL,
	login TINYTEXT NOT NULL,
	email TINYTEXT NOT NULL,
	password LONGTEXT NOT NULL,
	activated BIT NOT NULL DEFAULT 0,
	keyf VARCHAR(52))";
$req2 = $bdd->prepare($sql1);
$result1 = $req2->execute();
if ($result1)
	echo "Created users table\n<br>";
else
	echo "Erreur lors de la création de la table users \n";
$req2->closeCursor();

//Creation table MoreUsers
$sql2 = "CREATE TABLE IF NOT EXISTS MoreUsers (
	idMore INT AUTO_INCREMENT PRIMARY KEY,
	idUser int(11) NOT NULL,
	gender TINYTEXT NOT NULL,
	interest TINYTEXT NOT NULL,
	tags VARCHAR(250) DEFAULT '#RA9',
	Age int(11) NOT NULL DEFAULT 0,
	popularity int(11) NOT NULL DEFAULT 0,
	Bio VARCHAR(150) DEFAULT 'Hello, I am the android sent by Cyberlife',
	Updated int(11) NOT NULL DEFAULT 0,
	localisation VARCHAR(150) DEFAULT 'Detroit',
	country VARCHAR(150) DEFAULT 'United-States',
	latitude float NOT NULL DEFAULT '42.3314',
	longitude float NOT NULL DEFAULT '-83.04575',
	lastonline datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	isonline int(11) NOT NULL DEFAULT 0,
	FOREIGN KEY (`idUser`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`idUser`) REFERENCES `Users` (`id`));";
$req2 = $bdd->prepare($sql2);
$result2 = $req2->execute();
if ($result2)
	echo "Created MoreUsers table\n<br>";
else
	echo "Erreur lors de la création de la table MoreUsers \n";
$req2->closeCursor();
/*
//Creation table Tags
$sql3 = "CREATE TABLE IF NOT EXISTS Tags (
	idTag INT AUTO_INCREMENT PRIMARY KEY,
	idUsr int(11) NOT NULL,
	description TINYTEXT NOT NULL,
	FOREIGN KEY (`idUsr`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`idUsr`) REFERENCES `Users` (`id`))";
$req3 = $bdd->prepare($sql3);
$result3 = $req3->execute();
if ($result3)
	echo "Created Tags table\n<br>";
else
	echo "Erreur lors de la création de la table Tags \n";
$req3->closeCursor();
 */
//Creation table Images
$sql4 = "CREATE TABLE IF NOT EXISTS Images (
	idImg INT AUTO_INCREMENT PRIMARY KEY,
	idUser int(11) NOT NULL,
	image1 VARCHAR(250) DEFAULT '../imgs/plus.png',
	image2 VARCHAR(250) DEFAULT '../imgs/plus.png',
	image3 VARCHAR(250) DEFAULT '../imgs/plus.png',
	image4 VARCHAR(250) DEFAULT '../imgs/plus.png',
	image5 VARCHAR(250) DEFAULT '../imgs/plus.png',
	Profile VARCHAR(250) DEFAULT 'image1',
	FOREIGN KEY (`idUser`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`idUser`) REFERENCES `Users` (`id`))";
$req4 = $bdd->prepare($sql4);
$result4 = $req4->execute();
if ($result4)
	echo "Created Images table\n<br>";
else
	echo "Erreur lors de la création de la table Images \n";
$req4->closeCursor();


//Creation table ManageLike
$sql5 = "CREATE TABLE IF NOT EXISTS ManageLike (
	idLike INT AUTO_INCREMENT PRIMARY KEY,
	idUid int(11) NOT NULL,
	idLikeur int(11) NOT NULL,
	IsVisit int(11) DEFAULT 0,
	IsLike int(11) DEFAULT 0,
	IsMatcha int(11) DEFAULT 0,
	FOREIGN KEY (`idUid`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`idUid`) REFERENCES `Users` (`id`))";
$req5 = $bdd->prepare($sql5);
$result5 = $req5->execute();
if ($result5)
	echo "Created ManageLike table\n<br>";
else
	echo "Erreur lors de la création de la table ManageLike \n";
$req5->closeCursor();

//Creation table Notifications
$sql5 = "CREATE TABLE IF NOT EXISTS notifications (
	`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`emitter` int(11) NOT NULL,
	`receiver` int(11) NOT NULL,
	`text` varchar(255) NOT NULL,
	`seen` int(11) NOT NULL DEFAULT '0',
	FOREIGN KEY (`emitter`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`emitter`) REFERENCES `Users` (`id`),
	FOREIGN KEY (`receiver`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`receiver`) REFERENCES `Users` (`id`))";
$req5 = $bdd->prepare($sql5);
$result5 = $req5->execute();
if ($result5)
	echo "Created Notifications table\n<br>";
else
	echo "Erreur lors de la création de la table Notifications \n";
$req5->closeCursor();


//Creation table Temporary
$sql5 = "CREATE TABLE IF NOT EXISTS temporary (
	`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`idUser` int(11) NOT NULL,
	`interest` TINYTEXT NOT NULL,
	`distance` float NOT NULL DEFAULT 0,
	`tags` int(11) NOT NULL DEFAULT 0,
	`popu` int(11) NOT NULL DEFAULT '0',
	FOREIGN KEY (`idUser`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`idUser`) REFERENCES `Users` (`id`))";
$req5 = $bdd->prepare($sql5);
$result5 = $req5->execute();
if ($result5)
	echo "Crated Temporary table\n<br>";
else
	echo "Erreur lors de la création de la table Temporary \n";
$req5->closeCursor();


//Creation table Suggestions
$sql5 = "CREATE TABLE IF NOT EXISTS suggestions (
	`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`idUser` int(11) NOT NULL,
	`interest` TINYTEXT NOT NULL,
	`distance` float NOT NULL DEFAULT 0,
	`tags` int(11) NOT NULL DEFAULT 0,
	 `tagsin` VARCHAR(250) DEFAULT '#RA9',
	`popu` int(11) NOT NULL DEFAULT '0',
	`age` int(11) NOT NULL DEFAULT 0,
	FOREIGN KEY (`idUser`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`idUser`) REFERENCES `Users` (`id`))";
$req5 = $bdd->prepare($sql5);
$result5 = $req5->execute();
if ($result5)
	echo "Crated Suggestions table\n<br>";
else
	echo "Erreur lors de la création de la table Suggestions \n";
$req5->closeCursor();

//Creation table Blocked
$sql5 = "CREATE TABLE IF NOT EXISTS Blocked (
        `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`blockeur` int(11) NOT NULL,
	`blocked` int(11) NOT NULL,
	FOREIGN KEY (`blocked`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (`blocked`) REFERENCES `Users` (`id`),
        FOREIGN KEY (`blockeur`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (`blockeur`) REFERENCES `Users` (`id`))";
$req5 = $bdd->prepare($sql5);
$result5 = $req5->execute();
if ($result5)
        echo "Crated Blocked table\n<br>";
else
        echo "Erreur lors de la création de la table Blocked \n";
$req5->closeCursor();

//Creation table Message
$sql5 = "CREATE TABLE IF NOT EXISTS Message (
        `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `sender` int(11) NOT NULL,
	`receiver` int(11) NOT NULL,
	`text` VARCHAR(250) NOT NULL,
        FOREIGN KEY (`sender`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (`sender`) REFERENCES `Users` (`id`),
        FOREIGN KEY (`receiver`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (`receiver`) REFERENCES `Users` (`id`))";
$req5 = $bdd->prepare($sql5);
$result5 = $req5->execute();
if ($result5)
        echo "Crated Message table\n<br>";
else
        echo "Erreur lors de la création de la table Message \n";
$req5->closeCursor();

?>
